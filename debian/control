Source: john
Section: admin
Priority: optional
Maintainer: Kali Developers <devel@kali.org>
Uploaders: Sophie Brun <sophie@offensive-security.com>
XSBC-Original-Maintainer: Debian Security Tools <team+pkg-security@tracker.debian.org>
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13),
               dh-python,
               g++-11,
               libgmp-dev,
               libpcap0.8-dev,
               libssl-dev,
               python3,
               zlib1g-dev
Standards-Version: 4.6.2
Homepage: https://github.com/magnumripper/JohnTheRipper
XS-Debian-Vcs-Git: https://salsa.debian.org/pkg-security-team/john.git
XS-Debian-Vcs-Browser: https://salsa.debian.org/pkg-security-team/john
Vcs-Git: https://gitlab.com/kalilinux/packages/john.git
Vcs-Browser: https://gitlab.com/kalilinux/packages/john

Package: john
Architecture: any
Depends: john-data (= ${source:Version}), ${misc:Depends}, ${shlibs:Depends}
Suggests: wordlist
Description: active password cracking tool
 John the Ripper is a tool designed to help systems administrators to
 find weak (easy to guess or crack through brute force) passwords, and
 even automatically mail users warning them about it, if it is desired.
 .
 Besides several crypt(3) password hash types most commonly found on
 various Unix flavors, supported out of the box are Kerberos AFS and
 Windows NT/2000/XP/2003 LM hashes, plus several more with contributed
 patches.

Package: john-data
Architecture: all
Enhances: john
Depends: ${misc:Depends}, ${python3:Depends}
Breaks: john (<= 1.7.2-1)
Suggests: python3, ruby
Description: active password cracking tool - character sets
 John the Ripper is a tool designed to help systems administrators to
 find weak (easy to guess or crack through brute force) passwords, and
 even automatically mail users warning them about it, if it is desired.
 .
 This package contains architecture-independent character sets usable by
 john and architecture-independent scripts.
